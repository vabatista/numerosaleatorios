﻿namespace NumerosAleatorios
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumeroMax = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtQtdNumeros = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCarregar = new System.Windows.Forms.Button();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.txtSorteio = new System.Windows.Forms.TextBox();
            this.btnProximos = new System.Windows.Forms.Button();
            this.txtNumeroMin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rdIntervalo = new System.Windows.Forms.RadioButton();
            this.rdLista = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtListaNumeros = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtNumeroMax
            // 
            this.txtNumeroMax.Location = new System.Drawing.Point(213, 24);
            this.txtNumeroMax.Name = "txtNumeroMax";
            this.txtNumeroMax.Size = new System.Drawing.Size(46, 20);
            this.txtNumeroMax.TabIndex = 0;
            this.txtNumeroMax.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sortear números de: ";
            // 
            // txtQtdNumeros
            // 
            this.txtQtdNumeros.Location = new System.Drawing.Point(152, 156);
            this.txtQtdNumeros.Name = "txtQtdNumeros";
            this.txtQtdNumeros.Size = new System.Drawing.Size(46, 20);
            this.txtQtdNumeros.TabIndex = 2;
            this.txtQtdNumeros.Text = "10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Quantidade por sorteio:";
            // 
            // btnCarregar
            // 
            this.btnCarregar.Location = new System.Drawing.Point(81, 199);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(105, 25);
            this.btnCarregar.TabIndex = 4;
            this.btnCarregar.Text = "Carregar";
            this.btnCarregar.UseVisualStyleBackColor = true;
            this.btnCarregar.Click += new System.EventHandler(this.btnCarregar_Click);
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.Enabled = false;
            this.btnReiniciar.Location = new System.Drawing.Point(81, 230);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(105, 24);
            this.btnReiniciar.TabIndex = 5;
            this.btnReiniciar.Text = "Reiniciar";
            this.btnReiniciar.UseVisualStyleBackColor = true;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // txtSorteio
            // 
            this.txtSorteio.Location = new System.Drawing.Point(287, 25);
            this.txtSorteio.Multiline = true;
            this.txtSorteio.Name = "txtSorteio";
            this.txtSorteio.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSorteio.Size = new System.Drawing.Size(557, 259);
            this.txtSorteio.TabIndex = 6;
            this.txtSorteio.WordWrap = false;
            // 
            // btnProximos
            // 
            this.btnProximos.Enabled = false;
            this.btnProximos.Location = new System.Drawing.Point(430, 302);
            this.btnProximos.Name = "btnProximos";
            this.btnProximos.Size = new System.Drawing.Size(104, 27);
            this.btnProximos.TabIndex = 7;
            this.btnProximos.Text = "Próximos Números";
            this.btnProximos.UseVisualStyleBackColor = true;
            this.btnProximos.Click += new System.EventHandler(this.btnProximos_Click);
            // 
            // txtNumeroMin
            // 
            this.txtNumeroMin.Location = new System.Drawing.Point(140, 25);
            this.txtNumeroMin.Name = "txtNumeroMin";
            this.txtNumeroMin.Size = new System.Drawing.Size(46, 20);
            this.txtNumeroMin.TabIndex = 8;
            this.txtNumeroMin.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "até";
            // 
            // rdIntervalo
            // 
            this.rdIntervalo.Checked = true;
            this.rdIntervalo.Location = new System.Drawing.Point(13, 20);
            this.rdIntervalo.Name = "rdIntervalo";
            this.rdIntervalo.Size = new System.Drawing.Size(20, 24);
            this.rdIntervalo.TabIndex = 10;
            this.rdIntervalo.TabStop = true;
            this.rdIntervalo.UseVisualStyleBackColor = true;
            // 
            // rdLista
            // 
            this.rdLista.Location = new System.Drawing.Point(12, 50);
            this.rdLista.Name = "rdLista";
            this.rdLista.Size = new System.Drawing.Size(20, 24);
            this.rdLista.TabIndex = 11;
            this.rdLista.TabStop = true;
            this.rdLista.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Sortear números da lista abaixo: (separe por vírgula)";
            // 
            // txtListaNumeros
            // 
            this.txtListaNumeros.Location = new System.Drawing.Point(32, 72);
            this.txtListaNumeros.Multiline = true;
            this.txtListaNumeros.Name = "txtListaNumeros";
            this.txtListaNumeros.Size = new System.Drawing.Size(227, 78);
            this.txtListaNumeros.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 342);
            this.Controls.Add(this.txtListaNumeros);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rdLista);
            this.Controls.Add(this.rdIntervalo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNumeroMin);
            this.Controls.Add(this.btnProximos);
            this.Controls.Add(this.txtSorteio);
            this.Controls.Add(this.btnReiniciar);
            this.Controls.Add(this.btnCarregar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtQtdNumeros);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNumeroMax);
            this.Name = "Form1";
            this.Text = "Sorteio de Números Aleatórios";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumeroMax;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtQtdNumeros;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCarregar;
        private System.Windows.Forms.Button btnReiniciar;
        private System.Windows.Forms.TextBox txtSorteio;
        private System.Windows.Forms.Button btnProximos;
        private System.Windows.Forms.TextBox txtNumeroMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rdIntervalo;
        private System.Windows.Forms.RadioButton rdLista;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtListaNumeros;
    }
}

