﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace NumerosAleatorios
{
    public partial class Form1 : Form
    {

        private List<int> listaNumeros;
        private int qtdSorteio;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {
            qtdSorteio = Int16.Parse(txtQtdNumeros.Text);

            if (rdIntervalo.Checked)
            {
                int numMax = Int16.Parse(txtNumeroMax.Text);
                int numMin = Int16.Parse(txtNumeroMin.Text);
                listaNumeros = new List<int>(numMax-numMin);
                int i = numMin;
                while (i <= numMax)
                {
                    listaNumeros.Add(i);
                    i++;
                }
            }
            else if (rdLista.Checked)
            {
                string[] lista = txtListaNumeros.Text.Split(',');
                listaNumeros = new List<int>(lista.Length);
                foreach(string elemento in lista) {
                    listaNumeros.Add(Int16.Parse(elemento));
                }
            }

            Shuffle(listaNumeros);
            rdIntervalo.Enabled = false;
            rdLista.Enabled = false;
            txtNumeroMin.Enabled = false;
            txtListaNumeros.Enabled = false;
            txtQtdNumeros.Enabled = false;
            txtNumeroMax.Enabled = false;
            btnProximos.Enabled = true;
            btnReiniciar.Enabled = true;
            btnCarregar.Enabled = false;
        }


        private void Shuffle(IList<int> list)
        {
            var randomNumber = new Random(DateTime.Now.Millisecond);
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = randomNumber.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            txtQtdNumeros.Enabled = true;
            txtNumeroMax.Enabled = true;
            txtSorteio.Text = "";
            btnProximos.Enabled = false;
            btnReiniciar.Enabled = false;
            btnCarregar.Enabled = true;
            rdIntervalo.Enabled = true;
            rdLista.Enabled = true;
            txtNumeroMin.Enabled = true;
            txtListaNumeros.Enabled = true;

        }

        private void btnProximos_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < qtdSorteio; i++)
            {
                if (listaNumeros.Count > 0)
                {
                    txtSorteio.Text += listaNumeros[0].ToString() + "\t";
                    listaNumeros.RemoveAt(0);
                }
            }
            txtSorteio.Text += "\r\n\r";
            txtSorteio.Select(txtSorteio.Text.Length, 0);
            txtSorteio.ScrollToCaret();
            if (listaNumeros.Count == 0)
            {
                btnProximos.Enabled = false;
            }
        }
    }
}
